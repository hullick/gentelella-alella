import { IconDefinition } from '@fortawesome/fontawesome-common-types/index.d';

export interface Icon {
  faIcon: IconDefinition;
}
