import { Avatar } from './avatar';

export interface User {
  readonly name: string;
  readonly avatar: Avatar;
}
