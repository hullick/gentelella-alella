import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { Link } from './link';

export interface ItemMenuUserProfile extends Link {
  icon?: IconProp;
}
