import { User } from './user';

export interface Message {
  readonly sender: User;
  readonly content: string;
  readonly sentDate: Date;
}
