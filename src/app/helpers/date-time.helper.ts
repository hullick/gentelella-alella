import { Message } from '../classes/message';
export class DateTimeHelper {
  static getMessageDateDifferenceBetweenNow(message: Message): string {
    let timeDiff = new Date().getTime() - message.sentDate.getTime();

    if (timeDiff < 0) {
      throw new Error('The date could not be greather than now');
    } else {
      if (timeDiff < 1000) {
        return `${Math.floor(timeDiff)} milisecond(s) ago`;
      } else {
        timeDiff /= 1000;

        if (timeDiff < 60) {
          return `${Math.floor(timeDiff)} second(s) ago`;
        } else {
          timeDiff /= 60;

          if (timeDiff < 60) {
            return `${Math.floor(timeDiff)} minute(s) ago`;
          } else {
            timeDiff /= 60;

            if (timeDiff < 24) {
              return `${Math.floor(timeDiff)} hour(s) ago`;
            } else {
              timeDiff /= 24;

              if (timeDiff < 31) {
                return `${Math.floor(timeDiff)} day(s) ago`;
              } else {
                timeDiff /= 31;

                if (timeDiff < 12) {
                  return `${Math.floor(timeDiff)} month(s) ago`;
                } else {
                  timeDiff /= 12;

                  return `${Math.floor(timeDiff)} year(s) ago`;
                }
              }
            }
          }
        }
      }
    }
  }
}
