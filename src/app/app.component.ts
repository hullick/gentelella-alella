import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Message } from './classes/message';
import { User } from './classes/user';
import { messageReceived } from './store/actions/chat.actions';
import { userProfileChanged } from './store/actions/user-profile.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'GentelellA';

  constructor(private store: Store) {
    const currentUser: User = {
      name: 'Hullick Bartholo Gomes',
      avatar: {
        src: 'https://colorlib.com/polygon/gentelella/images/img.jpg',
      },
    };

    store.dispatch(userProfileChanged({ newUser: currentUser }));

    const message: Message = {
      content: 'Um exemplo de mensagem',
      sender: currentUser,
      sentDate: new Date('10/18/2021 23:25:20'),
    };

    new Array(10).fill(message).forEach((message) =>
      store.dispatch(
        messageReceived({
          newMessage: message,
        })
      )
    );
  }
}
