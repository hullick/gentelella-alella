import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { ReplaySubject } from 'rxjs';

@Component({
  template: '',
})
export abstract class GenericComponent implements AfterViewInit {
  @Input()
  classes: string = '';

  @Input()
  data: object = {};

  @ViewChild('rootElement')
  root!: ElementRef;

  protected rootElement$ = new ReplaySubject<ElementRef<any>>();

  constructor(protected _renderer: Renderer2) {}

  ngAfterViewInit(): void {
    if (this.root) {
      if (this.classes) {
        console.log(this.classes, this.root);
        this._renderer.addClass(this.root.nativeElement, this.classes);
      }

      Object.entries(this.data || {}).forEach((entry) => {
        const key = entry[0];
        const val = entry[1];

        this._renderer.setAttribute(
          this.root.nativeElement,
          `data-${key}`,
          val
        );
      });
    }
  }
}
