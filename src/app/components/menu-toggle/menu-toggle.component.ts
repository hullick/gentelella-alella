import { Component, OnInit, Renderer2 } from '@angular/core';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { SidedrawerState } from 'src/app/store/reducers/sidedrawer.reducer';
import { sidedraweInvertPreviousState } from '../../store/actions/sidedrawer.actions';
import { GenericComponent } from '../generic/generic.component';

@Component({
  selector: 'gentelella-menu-toggle',
  templateUrl: './menu-toggle.component.html',
  styleUrls: ['./menu-toggle.component.scss'],
})
export class MenuToggleComponent extends GenericComponent implements OnInit {
  faBars = faBars;
  sideDrawerOppened$!: Observable<SidedrawerState>;

  constructor(
    private store: Store<{ sidedrawerState: SidedrawerState }>,
    protected renderer: Renderer2
  ) {
    super(renderer);
    this.sideDrawerOppened$ = this.store.select('sidedrawerState');
  }

  ngOnInit(): void {}

  onToggleClicked(): void {
    this.store.dispatch(sidedraweInvertPreviousState());
  }
}
