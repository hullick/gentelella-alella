import { Component, OnInit, Renderer2 } from '@angular/core';
import { faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Message } from 'src/app/classes/message';
import { User } from 'src/app/classes/user';
import { ChatState } from 'src/app/store/reducers/chat.reducer';
import { UserProfileState } from 'src/app/store/reducers/user-profile.reducer';
import { GenericComponent } from '../generic/generic.component';

@Component({
  selector: 'gentelella-last-messages',
  templateUrl: './last-messages.component.html',
  styleUrls: ['./last-messages.component.scss'],
})
export class LastMessagesComponent extends GenericComponent implements OnInit {
  faAngleRight = faAngleRight;

  private chatState$!: Observable<ChatState>;
  private userProfileState$!: Observable<UserProfileState>;

  lastMessages!: Message[];
  currentUser!: User;
  collapsed: boolean = false;

  constructor(
    private storage: Store<{
      chatState: ChatState;
      userProfileState: UserProfileState;
    }>,
    protected renderer: Renderer2
  ) {
    super(renderer);

    this.chatState$ = storage.select('chatState');
    this.userProfileState$ = storage.select('userProfileState');
  }

  ngOnInit(): void {
    this.chatState$.subscribe((chatState) => {
      this.lastMessages = chatState.messages
        .slice(Math.max(chatState.messages.length - 5, 0))
        .reverse();

      this.collapsed = chatState.collapsed;
    });

    this.userProfileState$.subscribe((userProfileState) => {
      this.currentUser = userProfileState.userProfile!;
    });
  }

  onAllMessagesClick(event: Event) {
    event.stopPropagation();
    throw new Error('Not implemented yet!');
  }
}
