import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { UserProfileState } from 'src/app/store/reducers/user-profile.reducer';
import { ItemMenuUserProfile } from '../../classes/item-menu-user-profile';

@Component({
  selector: 'gentelella-menu-user-profile',
  templateUrl: './menu-user-profile.component.html',
  styleUrls: ['./menu-user-profile.component.scss'],
})
export class MenuUserProfileComponent implements OnInit {
  private userProfileState$!: Observable<UserProfileState>;

  collapsed!: boolean;

  menuItems?: ItemMenuUserProfile[] = [
    {
      href: 'javascript:;',
      text: `TESTE`,
      icon: { iconName: 'magnet', prefix: 'fas' },
    },
  ];

  loggoutItem?: ItemMenuUserProfile = {
    href: 'javascript:;',
    text: '<h4>TESTE</h4>',
    icon: { iconName: 'sign-out-alt', prefix: 'fas' },
  };

  constructor(private store: Store<{ userProfileState: UserProfileState }>) {
    this.userProfileState$ = store.select('userProfileState');
  }

  ngOnInit(): void {
    this.userProfileState$.subscribe((userProfileState) => {
      this.collapsed = userProfileState.userProfileMenuCollapsed;
    });
  }
}
