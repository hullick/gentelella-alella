import {
  Component,
  ElementRef,
  Input,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { ItemMenuUserProfile } from 'src/app/classes/item-menu-user-profile';
import { Link } from 'src/app/classes/link';
import { LinkComponent } from '../link/link.component';

@Component({
  selector: 'gentelella-item-menu-user-profile',
  templateUrl: './item-menu-user-profile.component.html',
  styleUrls: ['./item-menu-user-profile.component.scss'],
})
export class ItemMenuUserProfileComponent extends LinkComponent {
  @Input()
  itemMenuUserProfile!: ItemMenuUserProfile;

  constructor(protected _renderer: Renderer2) {
    super(_renderer);
  }

  getLinkInstance(): Link {
    const linkObject = {
      href: this.itemMenuUserProfile.href,
    };

    if (this.itemMenuUserProfile.icon) {
      return Object.assign(linkObject, {
        text: `<fa-icon [icon]="${JSON.stringify(
          this.itemMenuUserProfile.icon
        )}"></fa-icon>`,
      });
    } else {
      return Object.assign(linkObject, { text: this.itemMenuUserProfile.text });
    }
  }
}
