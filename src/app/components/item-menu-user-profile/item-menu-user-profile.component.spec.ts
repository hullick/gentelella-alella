import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemMenuUserProfileComponent } from './item-menu-user-profile.component';

describe('ItemMenuUserProfileComponent', () => {
  let component: ItemMenuUserProfileComponent;
  let fixture: ComponentFixture<ItemMenuUserProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemMenuUserProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemMenuUserProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
