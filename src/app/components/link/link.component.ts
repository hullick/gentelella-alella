import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnInit,
  Renderer2,
} from '@angular/core';
import { Link } from '../../classes/link';
import { GenericComponent } from '../generic/generic.component';

@Component({
  selector: 'gentelella-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.scss'],
})
export class LinkComponent extends GenericComponent implements OnInit {
  @Input()
  href!: string;

  constructor(protected _renderer: Renderer2) {
    super(_renderer);
  }

  ngOnInit(): void {}

  rootElement() {
    return this.root;
  }
}
