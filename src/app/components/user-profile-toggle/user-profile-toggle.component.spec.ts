import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserProfileToggleComponent } from './user-profile-toggle.component';

describe('UserProfileMenuComponent', () => {
  let component: UserProfileToggleComponent;
  let fixture: ComponentFixture<UserProfileToggleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserProfileToggleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileToggleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
