import { Component, OnInit } from '@angular/core';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { lastMessagesOpenChanger } from 'src/app/store/actions/chat.actions';
import { ChatState } from 'src/app/store/reducers/chat.reducer';

@Component({
  selector: 'gentelella-messages-toggle',
  templateUrl: './messages-toggle.component.html',
  styleUrls: ['./messages-toggle.component.scss'],
})
export class MessagesComponent implements OnInit {
  faEnvelope = faEnvelope;
  chatState$!: Observable<ChatState>;
  ariaExpanded: boolean = false;

  constructor(private store: Store<{ chatState: ChatState }>) {
    this.chatState$ = store.select('chatState');
  }

  ngOnInit(): void {
    this.chatState$.subscribe((currentChatState) => {
      this.ariaExpanded = currentChatState.collapsed;
    });
  }

  changeChatLasMessagesOpennedStatus() {
    this.store.dispatch(lastMessagesOpenChanger());
  }
}
