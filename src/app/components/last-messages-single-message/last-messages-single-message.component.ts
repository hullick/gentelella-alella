import { Component, Input, OnInit, Renderer2 } from '@angular/core';
import { Message } from 'src/app/classes/message';
import { DateTimeHelper } from 'src/app/helpers/date-time.helper';
import { GenericComponent } from '../generic/generic.component';

@Component({
  selector: 'gentelella-last-messages-single-message',
  templateUrl: './last-messages-single-message.component.html',
  styleUrls: ['./last-messages-single-message.component.scss'],
})
export class LastMessagesSingleMessageComponent
  extends GenericComponent
  implements OnInit
{
  @Input()
  message!: Message;

  dateTimeHelper = DateTimeHelper;

  constructor(protected renderer: Renderer2) {
    super(renderer);
  }

  ngOnInit(): void {}

  onMessageClick(event: Event) {
    event.stopPropagation();
    throw new Error('Not implemented yet!');
  }
}
