import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LastMessagesSingleMessageComponent } from './last-messages-single-message.component';

describe('LastMessagesSingleMessageComponent', () => {
  let component: LastMessagesSingleMessageComponent;
  let fixture: ComponentFixture<LastMessagesSingleMessageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LastMessagesSingleMessageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LastMessagesSingleMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
