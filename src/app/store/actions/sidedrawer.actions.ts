import { createAction } from '@ngrx/store';

export const sidedraweInvertPreviousState = createAction(
  '[SideDrawer Component] Invert Oppened State'
);
