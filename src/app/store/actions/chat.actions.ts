import { createAction, props } from '@ngrx/store';
import { Message } from 'src/app/classes/message';

export const CHAT_LAST_MESSAGES_COLLAPSED_CHANGER_TYPE =
  '[Chat System] Last Messages Collapsed State Changed';
export const CHAT_MESSAGE_RECEIVED_TYPE = '[Chat System] Message Received';
export interface CHAT_MESSAGE_RECEIVED_PROPS {
  newMessage: Message;
}

export const messageReceived = createAction(
  CHAT_MESSAGE_RECEIVED_TYPE,
  props<CHAT_MESSAGE_RECEIVED_PROPS>()
);

export const lastMessagesOpenChanger = createAction(
  CHAT_LAST_MESSAGES_COLLAPSED_CHANGER_TYPE
);
