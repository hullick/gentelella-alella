import { createAction, props } from '@ngrx/store';
import { User } from 'src/app/classes/user';

export const USER_PROFILE_MENU_OPEN_CHANGER_TYPE =
  '[User Profile] Menu Open State Changed';
export const USER_PROFILE_CHANGED_TYPE = '[User Profile] Profile Changed';

export interface USER_PROFILE_CHANGED_PROPS {
  newUser: User;
}

export const userProfileChanged = createAction(
  USER_PROFILE_CHANGED_TYPE,
  props<USER_PROFILE_CHANGED_PROPS>()
);

export const userProfileMenuOpenChanger = createAction(
  USER_PROFILE_MENU_OPEN_CHANGER_TYPE
);
