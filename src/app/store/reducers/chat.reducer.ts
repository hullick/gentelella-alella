import { createReducer, on } from '@ngrx/store';
import { Message } from '../../classes/message';
import {
  CHAT_MESSAGE_RECEIVED_PROPS,
  messageReceived,
  lastMessagesOpenChanger,
  CHAT_MESSAGE_RECEIVED_TYPE,
  CHAT_LAST_MESSAGES_COLLAPSED_CHANGER_TYPE,
} from '../actions/chat.actions';

export interface ChatState {
  messages: Message[];
  collapsed: boolean;
}

export const INITIAL_STATE: ChatState = {
  messages: [],
  collapsed: true,
};

const _chatMessagesReducer = createReducer(
  INITIAL_STATE,
  on(
    messageReceived,
    (state: ChatState, action: CHAT_MESSAGE_RECEIVED_PROPS) => {
      console.log(CHAT_MESSAGE_RECEIVED_TYPE);
      const newState = Object.assign({}, state);
      newState.messages = [
        ...newState.messages,
        (<CHAT_MESSAGE_RECEIVED_PROPS>action).newMessage,
      ];

      return newState;
    }
  ),
  on(lastMessagesOpenChanger, (state: ChatState) => {
    console.log(CHAT_LAST_MESSAGES_COLLAPSED_CHANGER_TYPE)
    const newState = Object.assign({}, state);
    newState.collapsed = !newState.collapsed;

    return newState;
  })
);

export function chatReducer(state: ChatState | undefined, action: any) {
  return _chatMessagesReducer(state, action);
}
