import { createReducer, on } from '@ngrx/store';
import { sidedraweInvertPreviousState } from '../actions/sidedrawer.actions';

export interface SidedrawerState {
  oppenedState: boolean;
}

export const INITIAL_STATE: SidedrawerState = {
  oppenedState: window.innerWidth > 991,
};

const _sideDrawerReducer = createReducer(
  INITIAL_STATE,
  on(sidedraweInvertPreviousState, (state: SidedrawerState) => ({
    oppenedState: !state.oppenedState,
  }))
);

export function sideDrawerReducer(state: SidedrawerState|undefined, action: any) {
  return _sideDrawerReducer(state, action);
}
