import { createReducer, on } from '@ngrx/store';
import { User } from '../../classes/user';
import {
  userProfileChanged,
  userProfileMenuOpenChanger,
  USER_PROFILE_CHANGED_PROPS,
  USER_PROFILE_CHANGED_TYPE,
  USER_PROFILE_MENU_OPEN_CHANGER_TYPE,
} from '../actions/user-profile.actions';

export interface UserProfileState {
  userProfile?: User;
  userProfileMenuCollapsed: boolean;
}

export const INITIAL_STATE: UserProfileState = {
  userProfileMenuCollapsed: false,
};

const _userProfileReducer = createReducer(
  INITIAL_STATE,
  on(
    userProfileChanged,
    (state: UserProfileState, action: USER_PROFILE_CHANGED_PROPS) => {
      console.log(USER_PROFILE_CHANGED_TYPE);

      const newState = Object.assign({}, state);

      newState.userProfile = action.newUser;

      return newState;
    }
  ),
  on(userProfileMenuOpenChanger, (state: UserProfileState) => {
    console.log(USER_PROFILE_MENU_OPEN_CHANGER_TYPE);

    const newState = Object.assign({}, state);
    newState.userProfileMenuCollapsed = !newState.userProfileMenuCollapsed;

    return newState;
  })
);

export function userProfileReducer(
  state: UserProfileState | undefined,
  action: any
) {
  return _userProfileReducer(state, action);
}
