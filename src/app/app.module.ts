import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MenuToggleComponent } from './components/menu-toggle/menu-toggle.component';
import { StoreModule } from '@ngrx/store';
import { sideDrawerReducer } from './store/reducers/sidedrawer.reducer';
import { chatReducer } from './store/reducers/chat.reducer';
import { MessagesComponent } from './components/messages-toggle/messages-toggle.component';
import { LastMessagesComponent } from './components/last-messages/last-messages.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserProfileToggleComponent } from './components/user-profile-toggle/user-profile-toggle.component';
import { userProfileReducer } from './store/reducers/user-profile.reducer';
import { ReactiveFormsModule } from '@angular/forms';
import { MenuUserProfileComponent } from './components/menu-user-profile/menu-user-profile.component';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ItemMenuUserProfileComponent } from './components/item-menu-user-profile/item-menu-user-profile.component';
import { LinkComponent } from './components/link/link.component';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { LastMessagesSingleMessageComponent } from './components/last-messages-single-message/last-messages-single-message.component';
import { TopnavComponent } from './components/topnav/topnav.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuToggleComponent,
    MessagesComponent,
    LastMessagesComponent,
    UserProfileToggleComponent,
    MenuUserProfileComponent,
    ItemMenuUserProfileComponent,
    LinkComponent,
    LastMessagesSingleMessageComponent,
    TopnavComponent,
  ],
  imports: [
    CollapseModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    StoreModule.forRoot({
      sidedrawerState: sideDrawerReducer,
      chatState: chatReducer,
      userProfileState: userProfileReducer,
    }),
    ReactiveFormsModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {

  constructor(library: FaIconLibrary){
    library.addIconPacks(fas, far, fab)
  }
}
